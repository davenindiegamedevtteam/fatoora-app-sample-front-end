package c.com.app.fatoorah.enduserpanel.offers.offerlist;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import c.com.app.fatoorah.enduserpanel.R;
import c.com.app.fatoorah.enduserpanel.offers.OfferScreenHeaderFragment;
import c.com.app.fatoorah.enduserpanel.offers.offerlist.OfferScreenListItemFragment.OnListFragmentInteractionListener;
import c.com.app.fatoorah.enduserpanel.offers.offerlist.content.ProductOfferContent;
import c.com.app.fatoorah.enduserpanel.offers.offerlist.content.ProductOfferContent.OfferItem;
import c.com.app.fatoorah.enduserpanel.offers.offerlist.filters.dialog.PreviewOfferDialogFragment;
import c.com.app.fatoorah.enduserpanel.offers.offerlist.wishlist.WishListCounter;

import java.util.List;

/** Generates list of offers available */
public class OfferScreenItemRecyclerViewAdapter extends RecyclerView.Adapter<OfferScreenItemRecyclerViewAdapter.ViewHolder> {

    private final List<OfferItem> mValues;
    private final OnListFragmentInteractionListener mListener;





    public OfferScreenItemRecyclerViewAdapter(List<OfferItem> items, OnListFragmentInteractionListener listener) {

        mValues = items;
        mListener = listener;

    }





    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_offer_screen_item, parent, false);

        return new ViewHolder(view);

    }





    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        // TODO: Need revision to fetch product name, info, and image per item of the list. See @ViewHolder class.

        holder.mItem = mValues.get(position);
        holder.mTvProductName.setText(mValues.get(position).productName);
        holder.mTvProductOffer.setText(mValues.get(position).details);
        OfferScreenHeaderFragment.setListCount(WishListCounter.numberOfOffersSaved());
//        holder.mImgProduct.setImageURI(); // --> Temporary close for now. This is where to display image from url.

        final int pos = position;

        // Check if user pressed the ADD button.
        holder.mBtnAddOffer.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                mValues.get(pos).toggleAdd();

                // List update and toggle button. (WISHLIST)
                if(mValues.get(pos).isAdded()) {

//                    holder.mBtnReduceOfferQty.setCompoundDrawableTintMode(PorterDuff.Mode.LIGHTEN);
//                    holder.mBtnAddOffer.setBackgroundColor(Color.CYAN);
                    holder.mBtnAddOffer.setColorFilter(Color.GREEN);
                    WishListCounter.raiseCount();
                    ProductOfferContent.addItemToWishlist(holder.mItem); // --> Add item here.

                } else if(!mValues.get(pos).isAdded()) {

//                    holder.mBtnReduceOfferQty.setCompoundDrawableTintMode(PorterDuff.Mode.DARKEN);
//                    holder.mBtnAddOffer.setBackgroundColor(Color.TRANSPARENT);
                    holder.mBtnAddOffer.setColorFilter(Color.BLACK);
                    WishListCounter.deductCount();
                    ProductOfferContent.removeItemToWishlist(holder.mItem, pos); // --> Remove

                }

                OfferScreenHeaderFragment.setListCount(WishListCounter.numberOfOffersSaved());

            }

        });

        // Refresh/update button status.
        if(mValues.get(pos).isAdded()) {

//            holder.mBtnReduceOfferQty.setCompoundDrawableTintMode(PorterDuff.Mode.LIGHTEN);
//            holder.mBtnAddOffer.setBackgroundColor(Color.CYAN);
            holder.mBtnAddOffer.setColorFilter(Color.GREEN);
//            WishListCounter.raiseCount();

        } else if(!mValues.get(pos).isAdded()) {

//            holder.mBtnReduceOfferQty.setCompoundDrawableTintMode(PorterDuff.Mode.DARKEN);
//            holder.mBtnAddOffer.setBackgroundColor(Color.TRANSPARENT);
            holder.mBtnAddOffer.setColorFilter(Color.BLACK);
//            WishListCounter.deductCount();

        }

        holder.mView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (null != mListener) {

                }

                FragmentManager fm = ((FragmentActivity)v.getContext()).getSupportFragmentManager();
                DialogFragment dialog = new PreviewOfferDialogFragment(mValues.get(position).productName, mValues.get(position).details);
                dialog.show(fm, "PREVIEW OFFER");

                Toast.makeText(v.getContext(), "PREVIEW OFFER of " + mValues.get(position).productName, Toast.LENGTH_SHORT).show();

            }

        });

    }





    @Override
    public int getItemCount() {
        return mValues.size();
    }





    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;

        public final TextView mTvProductName;
        public final TextView mTvProductOffer;
        public final ImageView mImgProduct;

        public final ImageButton mBtnAddOffer; // --> Button directly you to add offer you've selected to YOUR LIST/WISHLIST.

        public OfferItem mItem;

        public ViewHolder(View view) {

            super(view);

            mView = view;

            mTvProductName = (TextView) view.findViewById(R.id.tv_offer_product);
            mTvProductOffer = (TextView) view.findViewById(R.id.tv_offer_rebate);
            mImgProduct = (ImageView) view.findViewById(R.id.img_product);

            mBtnAddOffer = (ImageButton) view.findViewById(R.id.btn_add);

        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTvProductOffer.getText() + "'";
        }

    }





}
