package c.com.app.fatoorah.enduserpanel.homepage.offerlist.login;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import c.com.app.fatoorah.enduserpanel.R;
import c.com.app.fatoorah.enduserpanel.homepage.firstsetup.HomepageFirstSetupIntroFragment;

/** A signup screen under HOMEPAGE. */
public class HomepageSignupFragment extends Fragment { //--> TODO: Need email authentication here





    private Button btnSignup;
    private Button btnBack;

    private EditText etFirstname;
    private EditText etLastname;
    private EditText etEmail;
    private EditText etPassword;

    private TextView tvSignIn; // --> As a button
    private TextView tvError;





    public HomepageSignupFragment() {

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_homepage_signup, container, false);

        btnSignup = (Button) view.findViewById(R.id.btn_signup);
        btnBack = (Button) view.findViewById(R.id.btn_back);

        etFirstname = (EditText) view.findViewById(R.id.et_first_name);
        etLastname = (EditText) view.findViewById(R.id.et_last_name);
        etEmail = (EditText) view.findViewById(R.id.et_new_email);
        etPassword = (EditText) view.findViewById(R.id.et_new_password);

        tvSignIn = (TextView) view.findViewById(R.id.tv_btn_sign_in);
        tvError = (TextView) view.findViewById(R.id.tv_error);

        // BACK BUTTON
        btnBack.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick (View v){

                FragmentManager fm = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();

                ft.replace(R.id.fragment_homepage_main_content, new HomepageLoginFragment());
                ft.commit();

            }

        });

        // SIGN-UP BUTTON
        btnSignup.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick (View v){

                String firstname = etFirstname.getText().toString();
                String lastname = etLastname.getText().toString();
                String email = etEmail.getText().toString();
                String password = etPassword.getText().toString();

                // TODO: Do something to register new account.
                if(email.isEmpty() || password.isEmpty() || firstname.isEmpty() || lastname.isEmpty()) {

                    tvError.setText("Please fill it all up first.");
//                    return;

                }

                // Correct login will proceed to main account menu.
                FragmentManager fm = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.fragment_homepage_main_content, new HomepageFirstSetupIntroFragment());
                ft.commit();
            }

        });

        // SIGN-IN BUTTON
        tvSignIn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick (View v){

                FragmentManager fm = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();

                ft.replace(R.id.fragment_homepage_main_content, new HomepageLoginFragment());
                ft.commit();

            }

        });

        return view;

    }





}
