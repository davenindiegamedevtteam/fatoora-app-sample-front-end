package c.com.app.fatoorah.enduserpanel.offers.notifications.content;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class NotificationContent {





    // TODO: Revise it to display actual info real-time instead of dummy info. (Notification)





    public static final List<NotificationItem> ITEMS = new ArrayList<NotificationItem>();
    public static final Map<String, NotificationItem> ITEM_MAP = new HashMap<String, NotificationItem>();

    private static final int COUNT = 25;





    static {

        // Add some sample items.
        for (int i = 1; i <= COUNT; i++) {

            addItem(createDummyItem(i));

        }

    }





    private static void addItem(NotificationItem item) {

        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);

    }





    private static NotificationItem createDummyItem(int position) {

        return new NotificationItem(String.valueOf(position), "Mystery Item no. " + (position + 1), makeDummyDetails(), "9 mins ago");

    }





    private static String makeDummyDetails() {

        String[] msg = new String[15];
        msg[0] = "You received $99 from cash back.";
        msg[1] = "You received $88 from cash back.";
        msg[2] = "You received $45 from cash back.";
        msg[3] = "You received $32 from cash back.";
        msg[4] = "You received $66 from cash back.";
        msg[5] = "You received $77 from cash back.";
        msg[6] = "You received $69 from cash back.";
        msg[7] = "You received $19 from cash back.";
        msg[8] = "You received $18 from cash back.";
        msg[9] = "You received $17 from cash back.";
        msg[10] = "You received $20 from cash back.";
        msg[11] = "You received $11 from cash back.";
        msg[12] = "You received $44 from cash back.";
        msg[13] = "You received $33 from cash back.";
        msg[14] = "You received $299 from cash back.";

        StringBuilder builder = new StringBuilder();

        try {

            builder.append(msg[new Random().nextInt(15)]);

        } catch(IndexOutOfBoundsException e) {

            builder.append("A new offer awaits.");

        }

        return builder.toString();

    }





    public static class NotificationItem {

        public final String id;
        public final String title; // --> Topic/Sender
        public final String details; // --> Info
        public final String time; // --> TODO: Can you make real time update when does this message sent? N minutes ago? Days? You name it.

        public NotificationItem(String id, String title, String details, String time) {

            this.id = id;
            this.title = title;
            this.details = details;
            this.time = time;

        }

        @Override
        public String toString() {
            return title;
        }

    }





}
