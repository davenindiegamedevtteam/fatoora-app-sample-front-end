package c.com.app.fatoorah.enduserpanel.offers.offerlist.filters.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import c.com.app.fatoorah.enduserpanel.R;

public class PreviewOfferDialogFragment extends DialogFragment {





    private TextView tvOffername;
    private TextView tvCashback;

    private Button btnAdd;
    private Button btnCancel;

    private String offer;
    private String cash;





    public PreviewOfferDialogFragment (String offer, String cash) {

        this.offer = offer;
        this.cash = cash;

    }





    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View view = inflater.inflate(R.layout.dialog_fragment_offer_preview, null);

        builder.setView(view);
//                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//
//                });

        tvOffername = (TextView) ((View) view).findViewById(R.id.tv_preview_offer_name);
        tvCashback = (TextView) ((View) view).findViewById(R.id.tv_preview_offer_rebate);

        btnAdd = (Button) ((View) view).findViewById(R.id.btn_add);
        btnCancel = (Button) ((View) view).findViewById(R.id.btn_cancel);

        // ADD BUTTON
        btnAdd.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO: Can you help me out on how to ADD item to WISHLIST (YOUR LIST)?

            }

        });

        // CANCEL BUTTON
        btnCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                getDialog().dismiss();

            }

        });

        tvOffername.setText(offer);
        tvCashback.setText("Earn " + cash);

        return builder.create();

    }





}
