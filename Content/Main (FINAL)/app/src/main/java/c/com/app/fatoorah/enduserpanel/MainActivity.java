package c.com.app.fatoorah.enduserpanel;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import c.com.app.fatoorah.enduserpanel.offers.OfferScreenFragment;
import c.com.app.fatoorah.enduserpanel.offers.OfferScreenHeaderFragment;
import c.com.app.fatoorah.enduserpanel.offers.offerlist.wishlist.WishListCounter;
import c.com.app.fatoorah.enduserpanel.profile.FragmentProfile;
import c.com.app.fatoorah.enduserpanel.redeem.RedeemEmptyContentFragment;
import c.com.app.fatoorah.enduserpanel.redeem.RedeemScreenFragment;

public class MainActivity extends AppCompatActivity {





    private FragmentManager fm;
    private FragmentTransaction ft;





    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {

                case R.id.navigation_offer:
                    ft = fm.beginTransaction();
                    ft.replace(R.id.fragment_main, new OfferScreenFragment());
                    ft.commit();
                    OfferScreenHeaderFragment.setListCount(WishListCounter.numberOfOffersSaved());
                    return true;

                case R.id.navigation_redeem:
                    ft = fm.beginTransaction();
                    ft.replace(R.id.fragment_redeem_and_upload, new RedeemScreenFragment());
                    ft.commit();
                    return false;

                case R.id.navigation_profile:
                    ft = fm.beginTransaction();
                    ft.replace(R.id.fragment_main, new FragmentProfile());
                    ft.commit();
                    return true;

            }

            return false;

        }

    };





    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        RedeemEmptyContentFragment.navigation = navigation;

        fm = getSupportFragmentManager();

    }





}
