package c.com.app.fatoorah.enduserpanel.profile.subfragments.menu;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import c.com.app.fatoorah.enduserpanel.R;
import c.com.app.fatoorah.enduserpanel.profile.subfragments.FragmentProfileSubFragHeader;
import c.com.app.fatoorah.enduserpanel.profile.subfragments.menu.dialog.HelpFragment;

/** Profile Menu's fragment productName contains list of options. */
public class ProfileMenuFragment extends Fragment {





    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private View view;

    private Button btnChangeEmail;
    private Button btnLanguage;
    private Button btnSettings;
    private Button btnHelp;





    public ProfileMenuFragment() {

        // Required empty public constructor

    }





    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfileMenuFragment.
     */
    public static ProfileMenuFragment newInstance(String param1, String param2) {

        ProfileMenuFragment fragment = new ProfileMenuFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;

    }





    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if (getArguments() != null) {

            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

        }

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_profile_sub_frag_main_menu, container, false);

        btnSettings = (Button) view.findViewById(R.id.btn_settings);
        btnChangeEmail = (Button) view.findViewById(R.id.btn_change_email);
        btnLanguage = (Button) view.findViewById(R.id.btn_language_or_country);
        btnHelp = (Button) view.findViewById(R.id.btn_help_and_faq);

        // Reupdating PROFILE's header...
        FragmentManager fm = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fragment_header, new FragmentProfileSubFragHeader("Account", null, false));
        ft.commit();

        // CHANGE EMAIL MENU BUTTON
        btnChangeEmail.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                FragmentManager fm = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();

                ft.replace(R.id.fragment_content, new ChangeEmailFragment());
                ft.replace(R.id.fragment_header, new FragmentProfileSubFragHeader("Change Email", new ProfileMenuFragment(), true));
                ft.commit();

                Toast.makeText(view.getContext(), "CHANGE EMAIL MENU", Toast.LENGTH_SHORT).show();

            }

        });

        // CHANGE LANGUAGE MENU BUTTON
        btnLanguage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                FragmentManager fm = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();

                ft.replace(R.id.fragment_content, new LanguageFragment());
                ft.replace(R.id.fragment_header, new FragmentProfileSubFragHeader("Change Language", new ProfileMenuFragment(), true));
                ft.commit();

                Toast.makeText(view.getContext(), "LANGUAGE MENU", Toast.LENGTH_SHORT).show();

            }

        });

        // SETTINGS MENU BUTTON
        btnSettings.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                FragmentManager fm = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();

                ft.replace(R.id.fragment_content, new SettingsMenuFragment());
                ft.replace(R.id.fragment_header, new FragmentProfileSubFragHeader("Settings", new ProfileMenuFragment(), true));
                ft.commit();

                Toast.makeText(view.getContext(), "SETTINGS", Toast.LENGTH_SHORT).show();

            }

        });

        // HELP AND FAQ BUTTON
        btnHelp.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                FragmentManager fm = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
                DialogFragment dialog = new HelpFragment();
                dialog.show(fm, "HELP AND FAQ");

                Toast.makeText(view.getContext(), "HELP AND FAQ", Toast.LENGTH_SHORT).show();

            }

        });

        // Inflate the layout for this fragment
        return view;

    }





    // Returns error when you implement this on-click event straight to button via XML.
    public void testClick(View v) {

        Toast.makeText(view.getContext(), "show up FAQ", Toast.LENGTH_SHORT).show();

    }





}
