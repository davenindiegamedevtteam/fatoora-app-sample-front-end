package c.com.app.fatoorah.enduserpanel.homepage.offerlist;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import c.com.app.fatoorah.enduserpanel.R;
import c.com.app.fatoorah.enduserpanel.homepage.offerlist.offer.DummyContent;
import c.com.app.fatoorah.enduserpanel.homepage.offerlist.offer.DummyContent.DummyItem;
import c.com.app.fatoorah.enduserpanel.offers.offerlist.content.ProductOfferContent;

/** A list of offers shown only in HOMEPAGE. */
public class HomepageOfferItemFragment extends Fragment {





    private static final String ARG_COLUMN_COUNT = "column-count";
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;





    public HomepageOfferItemFragment() {

    }





    @SuppressWarnings("unused")
    public static HomepageOfferItemFragment newInstance(int columnCount) {

        HomepageOfferItemFragment fragment = new HomepageOfferItemFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;

    }





    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if (getArguments() != null) {

            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);

        }

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_homepage_offer_item_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {

            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;

            if (mColumnCount <= 1) {

                recyclerView.setLayoutManager(new LinearLayoutManager(context));

            } else {

                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));

            }

            recyclerView.setAdapter(new HompeageOfferItemRecyclerViewAdapter(ProductOfferContent.ITEMS, mListener));
        }

        return view;

    }





//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnListFragmentInteractionListener) {
//            mListener = (OnListFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnListFragmentInteractionListener");
//        }
//    }
//
//
//
//
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }





    public interface OnListFragmentInteractionListener {

        void onListFragmentInteraction(ProductOfferContent.OfferItem item);

    }





}
