package c.com.app.fatoorah.enduserpanel.redeem;


import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import c.com.app.fatoorah.enduserpanel.R;
import c.com.app.fatoorah.enduserpanel.offers.offerlist.wishlist.WishListCounter;

/** Starting category if user have none of the offers left on list. */
public class RedeemEmptyContentFragment extends Fragment {





    private Button btnAddOffers;
    public static BottomNavigationView navigation;





    public RedeemEmptyContentFragment() {

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_redeem_empty_content, container, false);

        btnAddOffers = (Button) view.findViewById(R.id.btn_add_offers);

        btnAddOffers.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // Set navigation to OFFER first.
                navigation.getMenu().getItem(0).setChecked(true);

                // Then update fragment.
                FragmentManager fm = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.fragment_main, new RedeemScreenFragment());
                ft.commit();

            }

        });

        return view;
    }





}
