package c.com.app.fatoorah.enduserpanel.profile.subfragments.menu;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;

import c.com.app.fatoorah.enduserpanel.R;
import c.com.app.fatoorah.enduserpanel.profile.subfragments.FragmentProfileSubFragHeader;
import c.com.app.fatoorah.enduserpanel.profile.subfragments.menu.dialog.settings.DeactivateAccountFragment;
import c.com.app.fatoorah.enduserpanel.profile.subfragments.menu.settingssubmenu.LegalAndPreferencesFragment;
import c.com.app.fatoorah.enduserpanel.profile.subfragments.menu.settingssubmenu.PushNotificationsFragment;
import c.com.app.fatoorah.enduserpanel.profile.subfragments.menu.settingssubmenu.ResetPasswordFragment;

/** A submenu under Profile menu. Contains options in managing user experience. */
public class SettingsMenuFragment extends Fragment {





    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    public Button btnPushNotif;
    public Button btnResetPassword;
    public Button btnLegalPref;
    public Button btnLogout;
    public Button btnDeactivate;

    public Switch sthAutoFlash;





    public SettingsMenuFragment() {

        // Required empty public constructor

    }





    public static SettingsMenuFragment newInstance(String param1, String param2) {

        SettingsMenuFragment fragment = new SettingsMenuFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;

    }





    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if (getArguments() != null) {

            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

        }

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_profile_sub_frag_settings_menu, container, false);

        btnPushNotif = (Button) view.findViewById(R.id.btn_push_notifications);
        btnResetPassword = (Button) view.findViewById(R.id.btn_reset_password);
        btnLegalPref = (Button) view.findViewById(R.id.btn_legal_preferences);
        btnLogout = (Button) view.findViewById(R.id.btn_logout);
        btnDeactivate = (Button) view.findViewById(R.id.btn_deactivate_account);

        sthAutoFlash = (Switch) view.findViewById(R.id.switch_auto_flash); //--> TODO: Do something to add logic stuff on auto-flash setting.

        // Back button redirect to PROFILE menu screen...
        FragmentManager fm = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fragment_header, new FragmentProfileSubFragHeader("Settings", new ProfileMenuFragment(), true));
        ft.commit();

        // PUSH NOTIFICATION
        btnPushNotif.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                FragmentManager fm = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();

                ft.replace(R.id.fragment_content, new PushNotificationsFragment());
                ft.replace(R.id.fragment_header, new FragmentProfileSubFragHeader("Push Notifications", new SettingsMenuFragment(), true));
                ft.commit();

            }

        });

        // RESET PASSWORD
        btnResetPassword.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                FragmentManager fm = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();

                ft.replace(R.id.fragment_content, new ResetPasswordFragment());
                ft.replace(R.id.fragment_header, new FragmentProfileSubFragHeader("Reset Password", new SettingsMenuFragment(), true));
                ft.commit();

            }

        });

        // AUTO-FLASH CAM MODE
        sthAutoFlash.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                // TODO: Make trigger for setting flash on/off

            }

        });

        // LEGAL AND PREFERENCES
        btnLegalPref.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                FragmentManager fm = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();

                ft.replace(R.id.fragment_content, new LegalAndPreferencesFragment());
                ft.replace(R.id.fragment_header, new FragmentProfileSubFragHeader("Legal and Preferences", new SettingsMenuFragment(), true));
                ft.commit();

            }

        });

        // LOGOUT
        btnLogout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO: Actual logout code here pls for logging out as replacement for test code.

                System.exit(0);

            }

        });

        // DEACTIVATE ACCOUNT
        btnDeactivate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                FragmentManager fm = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
                DialogFragment dialog = new DeactivateAccountFragment();
                dialog.show(fm, "DEACTIVATE ACCOUNT");

            }

        });

        // Inflate the layout for this fragment
        return view;

    }





}
