package c.com.app.fatoorah.enduserpanel.homepage.firstsetup;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;

import c.com.app.fatoorah.enduserpanel.MainActivity;
import c.com.app.fatoorah.enduserpanel.R;

public class HomepageFirstSetupBirthdayFragment extends Fragment {





    private Button btnNext;
    private Spinner spnBirthdate; //--> TODO: Need something to add calendar here.





    public HomepageFirstSetupBirthdayFragment() {

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_homepage_first_setup_birthday, container, false);

        btnNext = (Button) view.findViewById(R.id.btn_next);
        spnBirthdate = (Spinner) view.findViewById(R.id.spn_birthday);

        btnNext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO: DO something to update birthday.

                // Test mode for now.
                Context context = (FragmentActivity) view.getContext();
                ((FragmentActivity) view.getContext()).startActivity(new Intent(context, MainActivity.class));

            }

        });

        return view;

    }





}
