package c.com.app.fatoorah.enduserpanel.homepage.firstsetup;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;

import c.com.app.fatoorah.enduserpanel.R;

public class HomepageFirstSetupLocationFragment extends Fragment {





    // TODO: Define list of cities per country needed. Need logic...
    private Spinner spnCountry;
    private Spinner spnCity;





    private Button btnNext;





    public HomepageFirstSetupLocationFragment() {

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_homepage_first_setup_location, container, false);

        btnNext = (Button) view.findViewById(R.id.btn_next);

        spnCountry = (Spinner) view.findViewById(R.id.spn_birthday);
        spnCity = (Spinner) view.findViewById(R.id.spn_city); // --> TODO: Later revision. Elaborate list of cities per country. (Specific)

        btnNext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                FragmentManager fm = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();

                ft.replace(R.id.fragment_homepage_main_content, new HomepageFirstSetupNotificationFragment());
                ft.commit();

            }

        });

        return view;

    }





}
