package c.com.app.fatoorah.enduserpanel.offers.notifications;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import c.com.app.fatoorah.enduserpanel.R;
import c.com.app.fatoorah.enduserpanel.offers.notifications.NotificationsFragment.OnListFragmentInteractionListener;
import c.com.app.fatoorah.enduserpanel.offers.notifications.content.NotificationContent.NotificationItem;

import java.util.List;

public class NotificationsRecyclerViewAdapter extends RecyclerView.Adapter<NotificationsRecyclerViewAdapter.ViewHolder> {





    private final List<NotificationItem> mValues;
    private final OnListFragmentInteractionListener mListener;





    public NotificationsRecyclerViewAdapter(List<NotificationItem> items, OnListFragmentInteractionListener listener) {

        mValues = items;
        mListener = listener;

    }





    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_notification, parent, false);
        return new ViewHolder(view);

    }





    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.mItem = mValues.get(position);
        holder.mSender.setText(mValues.get(position).title);
        holder.mDetails.setText(mValues.get(position).details);
        holder.mTime.setText(mValues.get(position).time); // --> TODO: Update N minutes ago...

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // TODO: Directoy to the said notification's latest offer or do something to remove notification when reader is done checking.

                if (null != mListener) {

                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);

                }

            }
        });

    }





    @Override
    public int getItemCount() {
        return mValues.size();
    }





    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public final TextView mSender;
        public final TextView mDetails;
        public final TextView mTime;
        public NotificationItem mItem;

        public ViewHolder(View view) {

            super(view);
            mView = view;
            mSender = (TextView) view.findViewById(R.id.tv_sender);
            mDetails = (TextView) view.findViewById(R.id.tv_message);
            mTime = (TextView) view.findViewById(R.id.tv_time_when);

        }

        @Override
        public String toString() {
            return super.toString() + " '" + mDetails.getText() + "'";
        }

    }





}
