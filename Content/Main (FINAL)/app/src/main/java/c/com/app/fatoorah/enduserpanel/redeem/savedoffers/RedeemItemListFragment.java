package c.com.app.fatoorah.enduserpanel.redeem.savedoffers;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import c.com.app.fatoorah.enduserpanel.R;
import c.com.app.fatoorah.enduserpanel.offers.offerlist.content.ProductOfferContent;
import c.com.app.fatoorah.enduserpanel.offers.offerlist.content.ProductOfferContent.OfferItem;

/** A list of saved offers the user is selected. */
public class RedeemItemListFragment extends Fragment {





    /* TODO: Manage redeem saved offers list. Check for bugs. */





    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;





    public RedeemItemListFragment() {

    }





    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_redeem_wish_item_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {

            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;

            if (mColumnCount <= 1) {

                recyclerView.setLayoutManager(new LinearLayoutManager(context));

            } else {

                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));

            }

            recyclerView.setAdapter(new RedeemItemListRecyclerViewAdapter(ProductOfferContent.ITEMS_WISHLIST, mListener));

        }

        return view;

    }





//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnListFragmentInteractionListener) {
//            mListener = (OnListFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnListFragmentInteractionListener");
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {

        void onListFragmentInteraction(OfferItem item);

    }





}
