package c.com.app.fatoorah.enduserpanel.profile.subfragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import c.com.app.fatoorah.enduserpanel.R;

public class FragmentProfileSubFragHeader extends Fragment {





    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private String label;

    private View view;
    private TextView etLabel;
    private Button btnBack;
    private Fragment targetFragmentLayoutName;

    private boolean enableButton;





    /**
     * The header of the selected screen. allows you to
     * display name of the selected menu and has the back
     * button to redirect back to the previous menu screen.
     *
     * @param label The menu screen name to be displayed.
     * @param targetFragmentLayoutName The layout name you want to be displayed.
     * @param enableButton Simply hides the button.
     */
    public FragmentProfileSubFragHeader(String label, Fragment targetFragmentLayoutName, boolean enableButton) {

        this.label = label;
        this.targetFragmentLayoutName = targetFragmentLayoutName;
        this.enableButton = enableButton;

        String msg = "...";

        try {

            msg = "LABEL: " + this.label +
                    "\nFRAG:" + this.targetFragmentLayoutName.getClass().getSimpleName() +
                    "\nBUTTON ENABLED: " + Boolean.toString(this.enableButton);

        } catch(NullPointerException e) {

            msg = "LABEL: " + this.label +
                    "\nFRAG: Frag UKNOWN \nBUTTON ENABLED: " + Boolean.toString(this.enableButton);

        }

        Log.v("PROFILE SUB HEADER", msg);

    }





    public FragmentProfileSubFragHeader() {

        Log.v("PROFILE SUB HEADER", "Status empty");

    }





    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentProfileSubFragHeader.
     */
    public static FragmentProfileSubFragHeader newInstance(String param1, String param2) {

        FragmentProfileSubFragHeader fragment = new FragmentProfileSubFragHeader();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;

    }





    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if (getArguments() != null) {

            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

        }

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_profile_header_main, container, false);

        etLabel = (TextView) view.findViewById(R.id.tv_account_label);
        btnBack = (Button) view.findViewById(R.id.btn_back);

        etLabel.setText(label);

        if(enableButton) {

            btnBack.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    try {

                        FragmentManager fm = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
                        FragmentTransaction ft = fm.beginTransaction();

                        ft.replace(R.id.fragment_content, targetFragmentLayoutName);
                        ft.commit();

                    } catch(NullPointerException e) {

                        Toast.makeText(view.getContext(), "BUTTON returns NO FRAGMENT", Toast.LENGTH_SHORT).show();

                    }

                }

            });

        } else {

            btnBack.setVisibility(View.GONE);

        }

        return view;

    }





}
