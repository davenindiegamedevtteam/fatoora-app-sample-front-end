package c.com.app.fatoorah.enduserpanel.redeem;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import c.com.app.fatoorah.enduserpanel.R;
import c.com.app.fatoorah.enduserpanel.TestFragment3;
import c.com.app.fatoorah.enduserpanel.offers.offerlist.wishlist.WishListCounter;

/**
 * A simple {@link Fragment} subclass.
 */
public class RedeemScreenFragment extends Fragment {





    public Button btnBack;





    public RedeemScreenFragment() {

        // Required empty public constructor

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_redeem_screen, container, false);

        btnBack = (Button) view.findViewById(R.id.btn_back);

        // If user has more than 1 offer saved...
        if(WishListCounter.numberOfOffersSaved() >= 1) {

            FragmentManager fm = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.fragment_redeem_main_content, new RedeemMultiplyQuantityFragment());
            ft.commit();

        }

        // User pressed BACK button.
        btnBack.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                FragmentManager fm = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.fragment_redeem_and_upload, new TestFragment3());
                ft.commit();

            }

        });

        return view;

    }





}
