package c.com.app.fatoorah.enduserpanel.profile.subfragments.menu.settingssubmenu;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import c.com.app.fatoorah.enduserpanel.R;

/** A submenu under SETTINGS. Changes/updating your password. Nuff said. */
public class ResetPasswordFragment extends Fragment {





    private EditText etPassword;
    private EditText etReTypePassword;

    private Button btnReset;





    public ResetPasswordFragment() {

        // Required empty public constructor

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_settings_sub_reset_password, container, false);

        etPassword = (EditText) view.findViewById(R.id.et_enter_password);
        etReTypePassword = (EditText) view.findViewById(R.id.et_re_enter_password);

        btnReset = (Button) view.findViewById(R.id.btn_reset_password);

        btnReset.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO: DO something to change/update password here.

            }

        });

        return view;
    }





}
