package c.com.app.fatoorah.enduserpanel.offers.offerlist.filters;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.FitWindowsFrameLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.text.DecimalFormat;

import c.com.app.fatoorah.enduserpanel.R;

/** Search filter options for OFFER screen search result. */
public class OfferSearchFilterFragment extends Fragment {





    private static FitWindowsFrameLayout layout;

    private TextView tvAvailableOffers;
    private TextView tvBalance;

    private Button btnCancel;
    private Button btnApply;

    // TODO: Please replace it with 0 before initial release.
    public static int accountBalance = 9999999;
    public static int availableOffers = 999;

    public static DecimalFormat formatter;





    public OfferSearchFilterFragment() {

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_offer_screen_search_filter, container, false);

        layout = (FitWindowsFrameLayout) view.findViewById(R.id.filter_layout);
        layout.setVisibility(View.INVISIBLE); // --> FILTER hides from the start.

        tvAvailableOffers = (TextView) null; // --> TODO: Need something to update total offers available for every changes user made during filter screen.
        tvBalance = (TextView) view.findViewById(R.id.tv_account_balance); // --> TODO: Fetch account's total cash back balance to update.

        btnCancel = (Button) view.findViewById(R.id.btn_cancel);
        btnApply = (Button) view.findViewById(R.id.btn_apply);

        // Set value of balance here.
//        int value = accountBalance;
//        formatter = new DecimalFormat("#,###,###,###"); // --> TODO: Update format as you desired.
//        accountBalance = formatter.format(value);
//        tvBalance.setText("Cash Back: $" + Integer.toString(accountBalance));
//
//        // Set value of available offers here.
//        int value2 = availableOffers;
//        availableOffers = formatter.format(value2);
//        tvAvailableOffers.setText("Available offers: " + Integer.toString(availableOffers));

        // CANCEL BUTTON
        btnCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                layout.setVisibility(View.INVISIBLE);

            }

        });

        return view;

    }





    /** Get account user's balance here to display. */
    public static void setBalance(int value) {

//        formatter = new DecimalFormat("#,###,###,###"); // --> TODO: Update format as you desired.
//        accountBalance = formatter.format(value);

    }





    public static FitWindowsFrameLayout getLayout() {

        return layout;

    }





}
