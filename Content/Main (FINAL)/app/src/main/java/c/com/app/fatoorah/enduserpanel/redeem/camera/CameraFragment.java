package c.com.app.fatoorah.enduserpanel.redeem.camera;


import android.content.Context;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.Arrays;

import c.com.app.fatoorah.enduserpanel.R;
import c.com.app.fatoorah.enduserpanel.redeem.RedeemScreenFragment;

/**
 *
 * Camera fragment that uploads receipts.
 *
 * TODO: Revise/improvise Surface View to display actual cam screen. (Both this fragment and XML layout.)
 * TODO: Revise/improvise Texturee View to display actual cam screen for compatibility API 21 and up. (Both this fragment and XML layout.)
 *
 */
public class CameraFragment extends Fragment implements SurfaceHolder.Callback {





    private View view;

    // For Camera (Below API 21)...
    private SurfaceView svCamera;
    private SurfaceHolder holder;
    private Camera camOld; // --> NOTE: Using a deprecated camera as default camera.
    private boolean isCamRunning = false;

    // For Camera (API 21+)...
    private TextureView textureCamera; // --> API 21+ only
    private CameraDevice camNew; // --> Only API 21 and above.
    private Size previewsize;
    private Size jpegSizes[] = null;
    private CaptureRequest.Builder previewBuilder;
    private CameraCaptureSession previewSession;
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();

    private ImageButton btnCancel;
    private ImageButton btnTakePhoto;
    private ImageButton btnRetake;
    private ImageButton btnAddSection;
    private ImageButton btnFinish;

    private ConstraintLayout camLayout1;
    private ConstraintLayout camLayout2;

    private TextView camHint1;
    private TextView camHint2;





    static {

        ORIENTATIONS.append(Surface.ROTATION_0,90);
        ORIENTATIONS.append(Surface.ROTATION_90,0);
        ORIENTATIONS.append(Surface.ROTATION_180,270);
        ORIENTATIONS.append(Surface.ROTATION_270,180);

    }





    // NOTE: This callback is not working and throws an error if your device API is below 21. Commented out for now.
    // SOURCE: "https://stackoverflow.com/questions/27652236/noclassdeffounderror-in-android-studio"
//    private CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {
//
//        @Override
//        public void onOpened(CameraDevice camera) {
//
//            camNew = camera;
//            startCamera();
//
//        }
//
//        @Override
//        public void onDisconnected(CameraDevice camera) {
//
//        }
//
//        @Override
//        public void onError(CameraDevice camera, int error) {
//
//        }
//
//    };





    public CameraFragment() {

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_camera, container, false);

        btnCancel = (ImageButton) view.findViewById(R.id.btn_cancel);
        btnTakePhoto = (ImageButton) view.findViewById(R.id.btn_take_photo);
        btnRetake = (ImageButton) view.findViewById(R.id.btn_retake);
        btnAddSection = (ImageButton) view.findViewById(R.id.btn_add_section);
        btnFinish = (ImageButton) view.findViewById(R.id.btn_finish);

        camLayout1 = (ConstraintLayout) view.findViewById(R.id.camera_options_layout);
        camLayout2 = (ConstraintLayout) view.findViewById(R.id.camera_options_layout_2);

        camHint1 = (TextView) view.findViewById(R.id.tv_cam_tip_1);
        camHint2 = (TextView) view.findViewById(R.id.tv_cam_tip_2);

        camLayout2.setVisibility(View.INVISIBLE); // --> Temporarily hides the second cam options for now.

        // Default hint display
        camHint1.setText(R.string.receipt_hint_1_A);
        camHint2.setText(R.string.receipt_hint_1_B);

        // Camera Setup
        svCamera = (SurfaceView) view.findViewById(R.id.sv_camera);
        textureCamera = (TextureView) view.findViewById(R.id.texture_camera);
        holder = svCamera.getHolder();
        holder.addCallback(this);
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        // Version check compatibility for cam...
        if (Build.VERSION.SDK_INT >= 21) {

            svCamera.setVisibility(View.INVISIBLE);
            textureCamera.setVisibility(View.VISIBLE);

            // Displaying actual cam view...
            textureCamera.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {

                @Override
                public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {

                    CameraManager manager = (CameraManager) view.getContext().getSystemService(Context.CAMERA_SERVICE);

                    // Temporarily disabled via commented out for now due to API incompatibility.
                    try {

//                        String camerId=manager.getCameraIdList()[0];
//                        CameraCharacteristics characteristics = manager.getCameraCharacteristics(camerId);
//                        StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
//                        previewsize = map.getOutputSizes(SurfaceTexture.class)[0];
//                        manager.openCamera(camerId, stateCallback, null);

                    } catch (Exception e) {

                    }

                }

                @Override
                public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

                }

                @Override
                public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                    return false;
                }

                @Override
                public void onSurfaceTextureUpdated(SurfaceTexture surface) {

                }

            });

            Toast.makeText(view.getContext(),
                    "This SDK is compatible with latest cam system.", Toast.LENGTH_SHORT).show();

        } else {

            svCamera.setVisibility(View.VISIBLE);
            textureCamera.setVisibility(View.INVISIBLE);

            Toast.makeText(view.getContext(),
                    "This SDK is incompatible with latest cam system. (SDK " +
                            Build.VERSION.SDK_INT + ")", Toast.LENGTH_SHORT).show();

        }

        // CANCEL
        btnCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                FragmentManager fm = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.fragment_redeem_and_upload, new RedeemScreenFragment());
                ft.commit();

            }

        });

        // TAKE PHOTO
        btnTakePhoto.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO: Put logic somehow for taking photos and revise...

                camLayout2.setVisibility(View.VISIBLE);

                // Change hint...
                camHint1.setText(R.string.receipt_hint_2_A);
                camHint2.setText(R.string.receipt_hint_2_B);

            }

        });

        // RETAKE PHOTO
        btnRetake.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO: Put logic somehow for retaking photo in case of mistake.

                camLayout2.setVisibility(View.INVISIBLE);

                // Change hint again...
                camHint1.setText(R.string.receipt_hint_1_A);
                camHint2.setText(R.string.receipt_hint_1_B);

            }

        });

        // ADD SECTION
        btnAddSection.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO: Put logic here to take more screenshots before sending or uploading it with no. of pictures the user have.

                camLayout2.setVisibility(View.INVISIBLE);

                // Change hint again and again...
                camHint1.setText(R.string.receipt_hint_1_A);
                camHint2.setText(R.string.receipt_hint_1_B);

            }

        });

        // FINISH
        btnFinish.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO: Put logic here for uploading selected photos you've screenshot it when user pressed this.

                // After photo ready to upload, transitioning it to upload screen.
                FragmentManager fm = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.fragment_redeem_and_upload, new ReceiptUploadingFragment());
                ft.commit();

            }

        });

        return view;
    }





    @Override
    public void surfaceCreated(SurfaceHolder holder) {

        if (Build.VERSION.SDK_INT < 21) {

            camOld = Camera.open();

        }

    }





    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

        // NOTE: Using the int variable name "width" and "height" results in a GREEN screen error.

        if (Build.VERSION.SDK_INT < 21) {

            if(isCamRunning) {

                camOld.stopPreview();

            }

            Camera.Parameters p = camOld.getParameters();
            Display display = ((WindowManager) view.getContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();

//            p.setPreviewSize(width, height);
//            p.setPreviewSize(300, 400);

            // TODO: For back-end developers, please check on correct screen aspect ratio display to avoid distortion.
            int w = 400;
            int h = 300;

            if(display.getRotation() == Surface.ROTATION_0) {

                p.setPreviewSize(w, h);
                camOld.setDisplayOrientation(90);

            }

            if(display.getRotation() == Surface.ROTATION_90) {

                p.setPreviewSize(w, h);

            }

            if(display.getRotation() == Surface.ROTATION_180) {

                p.setPreviewSize(w, h);

            }

            if(display.getRotation() == Surface.ROTATION_270) {

                p.setPreviewSize(w, h);
                camOld.setDisplayOrientation(180);

            }

            camOld.setParameters(p);

            try {

                camOld.setPreviewDisplay(holder);

            } catch(IOException e) {

            }

            camOld.startPreview();
            isCamRunning = true;

        }

    }





    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

        if (Build.VERSION.SDK_INT < 21) {

            camOld.stopPreview();
            isCamRunning = false;
            camOld.release();

        }

    }





    /**
     *
     * For camera below API 21 only.
     *
     * TODO: Revise this for controlloing cam shots.
     *
     * */
    Camera.PictureCallback mPictureCallback = new Camera.PictureCallback() {

        public void onPictureTaken(byte[] imageData, Camera c) {

            // LOGIC HERE

        }

    };





    /** For API 21 and up only. (Camera2) */
    void  startCamera() {

        if(camNew == null || !textureCamera.isAvailable() || previewsize == null) {

            return;

        }

        SurfaceTexture texture = textureCamera.getSurfaceTexture();

        if(texture==null) {

            return;

        }

        texture.setDefaultBufferSize(previewsize.getWidth(),previewsize.getHeight());
        Surface surface= new Surface(texture);

        try {

            previewBuilder = camNew.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);

        } catch (Exception e) {

        }

        previewBuilder.addTarget(surface);

        try {

            camNew.createCaptureSession(Arrays.asList(surface), new CameraCaptureSession.StateCallback() {

                @Override
                public void onConfigured(CameraCaptureSession session) {

                    previewSession = session;
                    getChangedPreview();

                }

                @Override
                public void onConfigureFailed(CameraCaptureSession session) {

                }

            }, null);

        } catch (Exception e) {

        }

    }





    void getChangedPreview() {

        if(camNew == null) {

            return;

        }

        previewBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
        HandlerThread thread = new HandlerThread("changed Preview");
        thread.start();
        Handler handler = new Handler(thread.getLooper());

        try {

            previewSession.setRepeatingRequest(previewBuilder.build(), null, handler);

        } catch (Exception e){}

    }





}
